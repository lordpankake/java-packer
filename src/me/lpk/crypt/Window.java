package me.lpk.crypt;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import me.lpk.crypt.action.ActionChooseDestination;
import me.lpk.crypt.action.ActionChooseInput;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.BevelBorder;
import java.awt.SystemColor;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Window extends JFrame {
	private static final long serialVersionUID = 8631218969720885302L;
	private final Crypt crypt;
	private JPanel contentPane;
	private JTextField txtInput;
	private JTextField txtDestination;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window frame = new Window();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Window() {
		crypt = new Crypt();
		setTitle("Java Cryptor");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		txtInput = new JTextField();
		txtInput.setBounds(10, 11, 248, 20);
		contentPane.add(txtInput);
		txtInput.setColumns(10);

		JButton btnChooseInput = new JButton("Choose Input");
		btnChooseInput.addActionListener(new ActionChooseInput(crypt, txtInput));
		btnChooseInput.setBounds(268, 10, 166, 23);
		contentPane.add(btnChooseInput);

		txtDestination = new JTextField();
		txtDestination.setColumns(10);
		txtDestination.setBounds(10, 42, 248, 20);
		contentPane.add(txtDestination);

		JButton btnChooseDestination = new JButton("Choose Destination");
		btnChooseDestination.addActionListener(new ActionChooseDestination(crypt, txtDestination));
		btnChooseDestination.setBounds(268, 41, 166, 23);
		contentPane.add(btnChooseDestination);

		JPanel pnlOut = new JPanel();
		pnlOut.setBackground(SystemColor.menu);
		pnlOut.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		pnlOut.setBounds(10, 105, 424, 205);
		contentPane.add(pnlOut);
		pnlOut.setLayout(new BorderLayout(0, 0));

		JScrollPane scrllOut = new JScrollPane();
		pnlOut.add(scrllOut);

		JList<String> lstOutput = new JList<String>();
		lstOutput.setModel(new LogModel(crypt));
		scrllOut.setViewportView(lstOutput);

		JButton btnRun = new JButton("Run");
		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				crypt.run();
			}
		});
		btnRun.setBounds(268, 71, 166, 23);
		contentPane.add(btnRun);
	}
}
