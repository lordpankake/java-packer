package me.lpk.crypt;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;
import java.util.function.Predicate;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;

import me.lpk.crypt.ClassPath.ClassInfo;
import me.lpk.crypt.stub.Extractor;

public class Crypt {
	private File input, destination;
	private LogModel logging;

	/**
	 * Run the cryptor.
	 */
	public void run() {
		logging.clear();
		// Read classes from input
		// Log err and stop if input cannot be read.
		byte[] classes = null, resources = null;
		try {
			classes = writeToDat(input, s -> s.endsWith(".class"));
			resources = writeToDat(input, s -> !s.endsWith(".class") && !s.startsWith("META"));
		} catch (IOException e) {
			logging.err(e.getMessage());
			return;
		}
		// Begin creating output jar.
		try (JarOutputStream jos = new JarOutputStream(new FileOutputStream(destination))) {
			// Write input classes to destination
			String extractorName = Extractor.class.getName().replace(".", "/");
			String extractorSimple = Extractor.class.getSimpleName();
			String relative = extractorName.substring(0, extractorName.indexOf(extractorSimple));
			// Write input classes to destination
			jos.putNextEntry(new ZipEntry(relative + "classes.dat"));
			jos.write(classes);
			jos.closeEntry();
			// Write input resources to destination
			jos.putNextEntry(new ZipEntry(relative + "resources.dat"));
			jos.write(resources);
			jos.closeEntry();
			// Write stub classes to destination.
			Map<String, ClassInfo> stubClasses = ClassPath.getClasses(Extractor.class.getPackage().getName());
			for (String stubEntry : stubClasses.keySet()) {
				byte[] bytes = stubClasses.get(stubEntry).bytecode;
				jos.putNextEntry(new ZipEntry(stubEntry));
				jos.write(bytes);
				jos.closeEntry();
			}
			// Done, close output stream
			jos.close();
		} catch (IOException e) {
			logging.err(e.getMessage());
			return;
		}
		logging.info("Done!");
	}

	/**
	 * Set the logging output.
	 * 
	 * @param logging
	 */
	public void setLogging(LogModel logging) {
		this.logging = logging;
	}

	/**
	 * Set the input file to be crypted.
	 * 
	 * @param file
	 */
	public void setInput(File file) {
		input = file;
	}

	/**
	 * Set the destination for the crypted program.
	 * 
	 * @param file
	 */
	public void setDestination(File file) {
		destination = file;
	}

	/**
	 * Writes the given jar file to a byte array in an easy to extract format.
	 * 
	 * @param jarInput
	 *            Jar file to read files from.
	 * @param fileFilter
	 *            Filter to check names against. Non-passing names will not be
	 *            added to the byte array.
	 * @return
	 * @throws IOException
	 *             Thrown if an output stream could not be formed, or if input
	 *             file does not exist.
	 */
	private final static byte[] writeToDat(File jarInput, Predicate<String> fileFilter) throws IOException {
		byte[] data = null;
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			try (DataOutputStream dos = new DataOutputStream(baos)) {
				JarFile jar = new JarFile(jarInput);
				Enumeration<JarEntry> entries = jar.entries();
				while (entries.hasMoreElements()) {
					JarEntry entry = entries.nextElement();
					if (fileFilter.test(entry.getName())) {
						String entryName = entry.getName().replace("/", ".").replace(".class", "");
						byte[] entryBytes = Utils.toByteArray(jar.getInputStream(entry));
						// Write the entry name, the entry length, then the
						// entry data.
						// Makes for an extremely easy extraction later.
						dos.writeUTF(entryName);
						dos.writeInt(entryBytes.length);
						dos.write(entryBytes);
					}
				}
				jar.close();
			}
			data = baos.toByteArray();
		}
		return data;
	}

}
