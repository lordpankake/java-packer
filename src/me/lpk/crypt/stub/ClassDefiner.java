package me.lpk.crypt.stub;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Map;

/**
 * ClassLoader that defines classes and resources from a map of names to bytes.
 */
public final class ClassDefiner extends ClassLoader {
	/**
	 * Map of class names to their bytecode.
	 */
	private final Map<String, byte[]> classes;
	/**
	 * Map of resource names to their bytes.
	 */
	private final Map<String, byte[]> resources;

	/**
	 * Create the definer with the map of classes and resources to load from.
	 * 
	 * @param classes
	 * @param resources
	 */
	public ClassDefiner(Map<String, byte[]> classes, Map<String, byte[]> resources) {
		super(getSystemClassLoader());
		this.classes = classes;
		this.resources = resources;
	}

	@Override
	public final URL getResource(String name) {
		try {
			// Point URL to custom in-memroy handler
			return new URL(null, "bytes:///" + name, new ByteURLHandler());
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public final InputStream getResourceAsStream(final String name) {
		if (this.resources.containsKey(name)) {
			// Load from map of resources
			byte[] bytes = this.resources.get(name);
			return new ByteArrayInputStream(bytes);
		}
		// Default getResourceAsStream
		return super.getResourceAsStream(name);
	}

	@Override
	public final Class<?> findClass(String name) throws ClassNotFoundException {
		if (classes.containsKey(name)) {
			// Load from map of classes
			byte[] bytes = classes.get(name);
			return defineClass(name, bytes, 0, bytes.length, null);
		}
		// Default findClass
		return super.findClass(name);

	}

	/**
	 * URL stream handler for bytes.
	 */
	private final class ByteURLHandler extends URLStreamHandler {
		@Override
		protected URLConnection openConnection(URL u) throws IOException {
			return new ByteUrlConnection(u);
		}

		/**
		 * URL connection which points to raw ByteArrayInputStream.
		 */
		private final class ByteUrlConnection extends URLConnection {
			public ByteUrlConnection(URL url) {
				super(url);
			}

			@Override
			public final void connect() throws IOException {}

			@Override
			public final InputStream getInputStream() throws IOException {
				// Open stream to ClassDefiner's rescurces
				return new ByteArrayInputStream(resources.get(this.getURL().getPath().substring(1)));
			}
		}
	}
}