package me.lpk.crypt.stub;

import java.io.DataInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class Extractor {
	public static void main(String[] args) {
		Map<String, byte[]> classes = getMapOf("classes.dat");
		Map<String, byte[]> resources = getMapOf("resources.dat");
		try {
			ClassDefiner loader = new ClassDefiner(classes, resources);
			// Iterate read classes, load and invoke main method of main class.
			for (String name : classes.keySet()) {
				// Load class from entry
				Class<?> clazz = loader.loadClass(name);
				try {
					// Try to get the main method.
					// Will throw an exception if no such method exists
					Method main = clazz.getDeclaredMethod("main", String[].class);
					// Wrap args in object array to satisfy varargs of reflection invocation.
					Object[] argWrap = new Object[] { args };
					main.invoke(null, argWrap);
					return;
				} catch (NoSuchMethodException | SecurityException e) {
					// No main method exists in the class
				}
			}
			// If we've gotten here then no main method exists
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Read a map from the given data file.
	 * 
	 * @param dataFile
	 *            File to read from.
	 * @return
	 */
	private static Map<String, byte[]> getMapOf(String dataFile) {
		Map<String, byte[]> map = new HashMap<>();
		try (DataInputStream stream = new DataInputStream(Extractor.class.getResourceAsStream(dataFile))) {
			// Read classes from dat file
			while (stream.available() > 0) {
				String name = stream.readUTF();
				int contentLen = stream.readInt();
				byte[] content = new byte[contentLen];
				stream.readFully(content);
				map.put(name, content);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}
}
