package me.lpk.crypt.action;

import java.io.File;

import javax.swing.text.JTextComponent;

import me.lpk.crypt.Crypt;

public class ActionChooseInput extends AbstractFileAction {
	public ActionChooseInput(Crypt crypt, JTextComponent text) {
		super(crypt, text, true);
	}

	@Override
	protected void handleSelection(File file) {
		crypt.setInput(file);
	}
}
