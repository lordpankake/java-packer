package me.lpk.crypt.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;

import me.lpk.crypt.Crypt;

public abstract class AbstractFileAction implements ActionListener {
	private static final JFileChooser fileChooser;
	protected final Crypt crypt;
	private final JTextComponent text;
	private final boolean isInput;

	public AbstractFileAction(Crypt crypt, JTextComponent text, boolean isInput) {
		this.crypt = crypt;
		this.text = text;
		this.isInput = isInput;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// Open the file chooser, ask user for file input
		File file = getFileFromChooser();
		// If a file was selected, handle selection. Implementation handled by children.
		if (file != null) {
			text.setText(file.getPath());
			handleSelection(file);
		}
	}

	/**
	 * Called when the user selects a file.
	 * 
	 * @param file
	 *            File selected.
	 */
	protected abstract void handleSelection(File file);

	/**
	 * Opens the file chooser dialog.
	 * 
	 * @return File selected in dialog.
	 */
	protected File getFileFromChooser() {
		if (isInput)
			fileChooser.showOpenDialog(null);
		else
			fileChooser.showSaveDialog(null);
		return fileChooser.getSelectedFile();
	}

	static {
		// Create the file chooser and set the active directory to the current directory.
		// Apply a filter so only jar files can be selected.
		fileChooser = new JFileChooser(System.getProperty("user.dir"));
		fileChooser.setFileFilter(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.isDirectory() || file.getPath().toLowerCase().endsWith(".jar");
			}

			@Override
			public String getDescription() {
				return "Java Archives (JAR).";
			}
		});
	}

}
