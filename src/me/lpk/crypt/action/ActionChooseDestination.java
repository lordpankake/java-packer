package me.lpk.crypt.action;

import java.io.File;

import javax.swing.text.JTextComponent;

import me.lpk.crypt.Crypt;

public class ActionChooseDestination extends AbstractFileAction {
	public ActionChooseDestination(Crypt crypt, JTextComponent text) {
		super(crypt, text, false);
	}

	@Override
	protected void handleSelection(File file) {
		crypt.setDestination(file);
	}
}
