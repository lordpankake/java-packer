package me.lpk.crypt;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Written originally by Andy, modifications have been made from the original.
 * 
 * @author <a href="https://stackoverflow.com/a/19554704/8071915">Andy</a>
 */
public class ClassPath {
	public static class ClassInfo {
		public final String className;
		public final String resourceName;
		public final byte[] bytecode;

		public ClassInfo(String name, byte[] bytecode) {
			this.className = name;
			this.resourceName = name.replace(".", "/") + ".class";
			this.bytecode = bytecode;
		}
	}

	/**
	 * Retrieve all classes. Excludes JRE classes.
	 * 
	 * @return
	 */
	public static Map<String, ClassInfo> getAllClasses() {
		return getAllClasses(false);
	}

	/**
	 * Retrieve all classes.
	 * 
	 * @param includeJavaPath
	 *            Whether to include JRE classes in the map.
	 * @return
	 */
	public static Map<String, ClassInfo> getAllClasses(boolean includeJavaPath) {
		Map<String, ClassInfo> map = new HashMap<>();
		initSearch(map, includeJavaPath, s -> true);
		return map;
	}

	/**
	 * Retrieve all classes in the given package
	 * 
	 * @param pkg
	 *            The package name.
	 * @return
	 */
	public static Map<String, ClassInfo> getClasses(String pkg) {
		return getClasses(pkg, false, false);
	}

	/**
	 * Retrieve all classes in the given package. Excludes JRE classes.
	 * 
	 * @param pkg
	 *            The package name.
	 * @param recursive
	 *            If the sub-packages should be included.
	 * @return
	 */
	public static Map<String, ClassInfo> getClasses(String pkg, boolean recursive) {
		return getClasses(pkg, recursive, false);
	}

	/**
	 * Retrieve all classes in the given package.
	 * 
	 * @param pkg
	 * @param recursive
	 * @param includeJavaPath
	 * @return
	 */
	public static Map<String, ClassInfo> getClasses(final String pkg, final boolean recursive,
			final boolean includeJavaPath) {
		Map<String, ClassInfo> map = new HashMap<>();
		initSearch(map, includeJavaPath, name -> validName(recursive, pkg, name));
		return map;
	}

	/**
	 * Search the classpath, add classes that match the predicate into the given
	 * map.
	 * 
	 * @param map
	 *            Map to put results into.
	 * @param includeJavaPath
	 *            Whether to include JRE classes in the map.
	 * @param predicate
	 *            Predicate to check against class names.
	 */
	private static void initSearch(Map<String, ClassInfo> map, boolean includeJavaPath, Predicate<String> predicate) {
		String classpath = System.getProperty("java.class.path");
		String[] paths = classpath.split(System.getProperty("path.separator"));

		// Scan java JRE if desired.
		File javaLibs = new File(System.getProperty("java.home") + File.separator + "lib");
		if (includeJavaPath && javaLibs.exists()) {
			search(javaLibs, javaLibs, map, predicate);
		}
		// Scan elements in the classpath.
		for (String path : paths) {
			File file = new File(path);
			if (file.exists()) {
				search(file, file, map, predicate);
			}
		}
	}

	/**
	 * Search the given file to classes to put into the map, matching the
	 * predicate.
	 * 
	 * @param root
	 * @param file
	 * @param map
	 *            Map to put results into.
	 * @param filter
	 *            Predicate to check against class names.
	 * @return
	 */
	private static void search(File root, File file, Map<String, ClassInfo> map, Predicate<String> filter) {
		if (file.isDirectory()) {
			// Iterate sub-directories
			for (File child : file.listFiles()) {
				search(root, child, map, filter);
			}
		} else {
			try {
				// If file is a jar, read the classes inside and add all that
				// match the filter.
				if (file.getName().toLowerCase().endsWith(".jar")) {
					JarFile jar = null;
					try {
						jar = new JarFile(file);
					} catch (Exception ex) {}
					if (jar != null) {
						// Iterate all entries
						Enumeration<JarEntry> entries = jar.entries();
						while (entries.hasMoreElements()) {
							JarEntry entry = entries.nextElement();
							String name = entry.getName();
							int extIndex = name.lastIndexOf(".class");
							if (extIndex > 0) {
								String className = name.substring(0, extIndex).replace("/", ".");
								if (filter.test(className)) {
									ClassInfo info = new ClassInfo(className,
											Utils.toByteArray(jar.getInputStream(entry)));
									map.put(info.resourceName, info);
								}
							}
						}
					}
				} else if (file.getName().toLowerCase().endsWith(".class")) {
					// Iterating directory that contains classes, load from the
					// file.
					String className = createClassName(root, file);
					if (filter.test(className)) {
						ClassInfo info = new ClassInfo(className, Utils.toByteArray(new FileInputStream(file)));
						map.put(info.resourceName, info);
					}
				}
			} catch (Exception e) {}
		}
	}

	/**
	 * Re-creates the class name of the given file by going back through the
	 * parents, prepending each directory as part of the package.
	 * 
	 * @param root
	 * @param file
	 *            Class file
	 * @return
	 */
	private static String createClassName(File root, File file) {
		StringBuffer sb = new StringBuffer();
		String fileName = file.getName();
		sb.append(fileName.substring(0, fileName.lastIndexOf(".class")));
		file = file.getParentFile();
		while (file != null && !file.equals(root)) {
			sb.insert(0, '.').insert(0, file.getName());
			file = file.getParentFile();
		}
		return sb.toString();
	}

	/**
	 * Checks if the name is valid by comparing it to the given package.
	 * 
	 * @param recursive
	 * @param pkg
	 * @param name
	 * @return
	 */
	private static boolean validName(boolean recursive, String pkg, String name) {
		int i = name.lastIndexOf(".");
		String classPackage = (i == -1) ? "" : name.substring(0, i);
		return ((recursive && classPackage.startsWith(pkg)) || (!recursive && pkg.equals(classPackage)));
	}
}