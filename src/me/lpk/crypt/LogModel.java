package me.lpk.crypt;

import javax.swing.DefaultListModel;

public class LogModel extends DefaultListModel<String> {
	private static final long serialVersionUID = -5187681596615534000L;
	private static final String PRE_INFO = "[INFO] ";
	private static final String PRE_ERR = "[ERR]  ";
	private static final String PRE_WARN = "[WARN] ";

	public LogModel(Crypt crypt) {
		crypt.setLogging(this);
	}

	/**
	 * Log an informative message.
	 * 
	 * @param message
	 */
	public void info(String message) {
		this.addElement(PRE_INFO + message);
	}

	/**
	 * Log an error message.
	 * 
	 * @param message
	 */
	public void err(String message) {
		this.addElement(PRE_ERR + message);
	}

	/**
	 * Log an warning message.
	 * 
	 * @param message
	 */
	public void warn(String message) {
		this.addElement(PRE_WARN + message);
	}

}
