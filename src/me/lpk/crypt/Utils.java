package me.lpk.crypt;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Utils {

	/**
	 * Reads the bytes from the given InputStream.
	 * 
	 * @param is
	 *            InputStream to read from.
	 * @return
	 * @throws IOException
	 *             Thrown if the input cannot be read or if the temporary output stream could not be written to.
	 */
	public final static byte[] toByteArray(InputStream input) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		int n = 0;
		byte[] buffer = new byte[1024];
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
		}
		return output.toByteArray();
	}
}
